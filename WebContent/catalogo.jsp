<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<sql:query var="modelos" dataSource="jdbc/classicmodels">
 select productName, productScale, productDescription from products;
</sql:query>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
</head>
<body>
	<h1>Catálogo de Modelos a Escala</h1>
	<hr/>
	<%
		String customer = (String) session.getAttribute("customer");
		if (customer != null){
	%>
	<p><%=customer%></p>
	<p><a href=logout.jsp>Iniciar Sesion</a></p>
	<%
		} else { 
	%>
	<p><a href=login.jsp>Iniciar Sesion</a></p>
	<%
		}
	%>
	<c:forEach var="modelo" items="${modelos.rows}">
		<h3>
			<c:out value="${modelo.productName}" />
		</h3>
		<p>
			Escala
			<c:out value="${modelo.productScale}" />
		</p>
		<p>
			<c:out value="${modelo.productDescription}" />
		</p>
		<hr />
	</c:forEach>
</body>
</html>
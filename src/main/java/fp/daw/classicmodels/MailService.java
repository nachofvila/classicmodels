package fp.daw.classicmodels;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailService {

	public static void sendMessage(String smtpServer, String port, String account, String password, String to,
			String subject, String body, String type) throws MessagingException {
		Properties properties = new Properties();
		// Servidor SMTP.
		properties.setProperty("mail.smtp.host", smtpServer);
		// Habilitar TLS.
		properties.setProperty("mail.smtp.starttls.enable", "true");
		// Puerto para env�o de correos.
		properties.setProperty("mail.smtp.port", port);
		// Si requiere usuario y password para conectarse.
		if (account != null && password != null)
			properties.setProperty("mail.smtp.auth", "true");
		// Obtener un objeto Session con las propiedades establecidas.
		Session mailSession = Session.getInstance(properties);
		// Para obtener un log de salida m�s extenso.
		mailSession.setDebug(true);
		Message message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(account));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(subject);
		message.setContent(body, type);
		message.setSentDate(new Date());
		Transport.send(message, account, password);
	}
}